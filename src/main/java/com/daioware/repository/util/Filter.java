package com.daioware.repository.util;

public class Filter {
	static enum Type{
		EQUALS,
		DIFFERENT,
		CONTAINS,
		STARTS_WITH,
		ENDS_WITH,
		HIGHER,
		LOWER,
		HIGHER_OR_EQUALS,
		LOWER_OR_EQUALS,
	}
	
	private String field;
	private Object value;
	private Type type;
	private String operand;
	
	
	public Filter(String field, Object value) {
		this(field,value,null,"&&");
	}
	public Filter(String field, Object value, Type type) {
		this(field,value,type,"&&");
	}
	
	public Filter(String field, Object value, Type type,String operand) {
		setField(field);
		setValue(value);
		setType(type);
		setOperand(operand);
	}

	public Filter(Filter f) {
		this(f.getField(),f.getValue(),f.getType(),f.getOperand());
	}
	public Filter clone() {
		return new Filter(this);
	}
	public String getOperand() {
		return operand;
	}

	public void setOperand(String operand) {
		this.operand = operand;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
}
