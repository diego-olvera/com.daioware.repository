package com.daioware.repository.util;

import static com.daioware.repository.util.Filter.Type.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.daioware.language.compiler.InvalidSymbolException;
import com.daioware.language.compiler.LexicalAnalyzer;
import com.daioware.language.compiler.Symbol;
import com.daioware.repository.util.Filter.Type;

public class FilterList implements Iterable<Filter>{
	private List<Filter> filters=new LinkedList<>();
	
	public FilterList() {
	}
	public void clear() {
		filters=new LinkedList<>();
	}
	
	public List<Filter> getFilters() {
		return filters;
	}
	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}
	public void setQuery(String query)throws InvalidSymbolException {
		LexicalAnalyzer lex=new LexicalAnalyzer(query);
		lex.parse();
		lex.removeWhiteSpaces();
		List<Symbol> symbols=lex.getSymbols();
		ListIterator<Symbol>it=symbols.listIterator();
		Symbol symbol;
		String text;
		while(it.hasNext()) {
			symbol=it.next();
			text=symbol.getText();
			if(symbol.isType(Symbol.Type.IDENTIFIER)) {
				it.previous();
				add(new Filter(getCompoundField(it),null));
				symbol=it.next();
				text=symbol.getText();
				switch(text) {
					case "[":op("[]", it.next().getText());break;
					case "<":
						if(it.next().getText().equals(">")){
							op("<>",it.next().getText());
							break;
						}
						else {
							it.previousIndex();
							it.previousIndex();
						}
					default:
						op(text,it.next().getText());
				}
			}			
			else if(text.equals("(")) {
				do {
					symbol=it.next();
					op(text,symbol);
				}while(!symbol.isType(Symbol.Type.IDENTIFIER));
				it.previous();
			}
			else if(text.equals(")")) {
				op(text,symbol);
				symbol=null;
				while(it.hasNext()) {
					if((symbol=it.next()).isType(Symbol.Type.CLOSE_PARENTHESIS)) {
						System.out.println("s:"+symbol.getText());
						op(symbol.getText(),symbol);
						symbol=null;
					}
					else {
						symbol=it.previous();
						break;
					}
				}
				if(symbol!=null) {
					System.out.println("t:"+symbol.getText());
					op(symbol.getText(),it.next().getText());
				}
			}
		}
	}
	protected String getCompoundField(ListIterator<Symbol> iterator) {
		StringBuilder info=new StringBuilder();
		Symbol symbol;
		String prefix="";
		do {
			symbol=iterator.next();
			info.append(prefix).append(symbol.getText());
			prefix=".";
			symbol=iterator.next();
		}while(symbol.getText().equals("."));
		iterator.previous();
		return info.toString();
	}
	public FilterList field(String name) {
		add(new Filter(name, null, null));
		return this;
	}
	
	public FilterList equal(Object... value) {
		return multiOp(EQUALS, value);
	}
	public FilterList diff(Object... value) {
		return multiOp(Type.DIFFERENT, value);
	}
	public FilterList startsWith(Object... value) {
		return multiOp(Type.STARTS_WITH, value);
	}
	public FilterList endsWith(Object... value) {
		return multiOp(Type.ENDS_WITH, value);
	}
	public FilterList higherOrEquals(Object... value) {
		return multiOp(Type.HIGHER_OR_EQUALS, value);
	}
	public FilterList higher(Object... value) {
		return multiOp(Type.HIGHER, value);
	}
	public FilterList lowerOrEquals(Object... value) {
		return multiOp(Type.LOWER_OR_EQUALS, value);
	}
	public FilterList lower(Object... value) {
		return multiOp(Type.LOWER, value);
	}
	protected Filter getLast() {
		return filters.get(filters.size()-1);
	}
	public FilterList betweenOrEquals(Object low,Object high) {
		Filter lowFilter=getLast();
		lowerOrEquals(low);
		lowFilter.setOperand("&&");
		add(new Filter(lowFilter.getField(),high));
		higherOrEquals(high);
		return this;
	}
	public FilterList between(Object low,Object high) {
		Filter lowFilter=getLast();
		lower(low);
		lowFilter.setOperand("&&");
		add(new Filter(lowFilter.getField(),high));
		higher(high);
		return this;
	}
	public FilterList add(String name,Object value,Type t) {
		return add(new Filter(name, value, t));
	}
	public FilterList add(Filter filter) {
		filters.add(filter);
		return this;
	}
	public FilterList and() {
		getLast().setOperand("&&");
		return this;
	}
	public FilterList or() {
		getLast().setOperand("||");
		return this;
	}
	public FilterList open() {
		filters.add(new Filter("(",null));
		return this;
	}
	public FilterList close() {
		getLast().setOperand(null);
		filters.add(new Filter(")",null));
		return this;
	}
	public FilterList add(Filter filter,String operand) {
		Filter f=getLast();
		f.setOperand(operand);
		return add(filter);
	}
	public String getCode() {
		StringBuilder info=new StringBuilder();
		int currentIndex=0,size=filters.size();
		String field,operand;
		for(Filter filter:filters) {
			field=filter.getField();
			if(field.equals("(") || field.equals(")")) {
				info.append(field);
			}
			else {
				info.append(field)
					.append(" ")
					.append(filter.getType())
					.append(" ")
					.append(filter.getValue());
				operand=filter.getOperand();
				if(++currentIndex<size && operand!=null) {
					info.append(" ").append(filter.getOperand()).append(" ");
				}
			}
		}
		return info.toString();
	}
	public FilterList op(String operator,Object...values) {
		switch(operator) {
		case "&&":
			return and();
		case "||":
			return or();
		case "=":
			return equal(values);
		case "^":
			return startsWith(values);
		case "$":
			return endsWith(values);
		case "[]":
			return endsWith(values);
		case "<>":
			return diff(values);
		case ">":
			return higher(values);
		case "<":
			return lower(values);
		case ">=":
			return higherOrEquals(values);
		case "<=":
			return lowerOrEquals(values);
		case "(":
			return open();
		case ")":
			return close();
		}
		return this;
	}
	public FilterList multiOp(Type type,Object...values) {
		Filter f=getLast();
		int it=0,size=values.length;
		f.setValue(values[it]);
		f.setType(type);
		it++;
		for(;it<size;it++) {
			f=getLast();
			f.setOperand("||");
			f=f.clone();
			f.setValue(values[it]);
		}
		return this;
	}
	@Override
	public Iterator<Filter> iterator() {
		return filters.iterator();
	}
	protected static void simpleTest() {
		FilterList filter=new FilterList();
		filter.field("name").equal("Diego")
				.or().field("age").betweenOrEquals(1,10)
				.and().field("field1").diff(10)
				.open().field("number_field").equal(10).close();
		System.out.println(filter.getCode());
	}
	protected static void complexText() {
		FilterList filter=new FilterList();
		String query="user.name$\"Diego\" && s=10 || s<=10 && (d=1) && s<>10";
		try {
			System.out.println("original query:"+query);
			filter.setQuery(query);
			for(Object o:new LexicalAnalyzer(query).parse()) {
				System.out.println(o);
			}
			System.out.println(filter.getCode());

		} catch (InvalidSymbolException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		complexText();
	}
}
