package com.daioware.repository.util;

import com.daioware.commons.CompleteDataFormat;

public class IdElement<T> implements CompleteDataFormat{
	protected T id;
	
	public IdElement() {}
	public IdElement(T id) {
		setId(id);
	}
	public T getId() {
		return id;
	}
	public void setId(T id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return getId().toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof IdElement))
			return false;
		IdElement<T> other = (IdElement<T>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
	
}
