package com.daioware.repository.util;

public interface IdItem<T> {
	T getId();
	void setId(T x);
}
