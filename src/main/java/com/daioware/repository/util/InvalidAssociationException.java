package com.daioware.repository.util;

public class InvalidAssociationException extends InvalidAttributeException{
	private static final long serialVersionUID = 1L;

	public InvalidAssociationException() {
		super();
	}

	public InvalidAssociationException(String message, int which, int scope) {
		super(message, which, scope);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(String message, int which) {
		super(message, which);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace, int which, int scope) {
		super(message, cause, enableSuppression, writableStackTrace, which, scope);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace, int which) {
		super(message, cause, enableSuppression, writableStackTrace, which);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(String message, Throwable cause, int which, int scope) {
		super(message, cause, which, scope);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(String message, Throwable cause, int which) {
		super(message, cause, which);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(Throwable cause, int which, int scope) {
		super(cause, which, scope);
		// TODO Auto-generated constructor stub
	}

	public InvalidAssociationException(Throwable cause, int which) {
		super(cause, which);
		// TODO Auto-generated constructor stub
	}



}
