package com.daioware.repository.util;

public class InvalidAttributeException extends SaveException{
	private static final long serialVersionUID = 1L;
	
	private int scope;
	private int which;
	
	public InvalidAttributeException() {
		super();
	}

	public InvalidAttributeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace,int which) {
		super(message, cause, enableSuppression, writableStackTrace);
		setWhich(which);
	}

	public InvalidAttributeException(String message, Throwable cause,int which) {
		super(message, cause);
		setWhich(which);
	}

	public InvalidAttributeException(String message,int which) {
		super(message);
		setWhich(which);
	}

	public InvalidAttributeException(Throwable cause,int which) {
		super(cause);
		setWhich(which);
	}

	//
	
	
	public InvalidAttributeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace,int which,int scope) {
		super(message, cause, enableSuppression, writableStackTrace);
		setWhich(which);
		setScope(scope);
	}

	public InvalidAttributeException(String message, Throwable cause,int which,int scope) {
		super(message, cause);
		setWhich(which);
		setScope(scope);
	}

	public InvalidAttributeException(String message,int which,int scope) {
		super(message);
		setWhich(which);
		setScope(scope);
	}

	public InvalidAttributeException(Throwable cause,int which,int scope) {
		super(cause);
		setWhich(which);
		setScope(scope);
	}
	public int getWhich() {
		return which;
	}

	public void setWhich(int which) {
		this.which = which;
	}

	public int getScope() {
		return scope;
	}

	public void setScope(int scope) {
		this.scope = scope;
	}

}
