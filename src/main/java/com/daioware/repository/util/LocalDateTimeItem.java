package com.daioware.repository.util;

import java.time.LocalDateTime;

public interface LocalDateTimeItem {

	void setInsertionDate(LocalDateTime date);
	LocalDateTime getInsertionDate();
	void setUpdateDate(LocalDateTime date);
	LocalDateTime getUpdateDate();
}
