package com.daioware.repository.util;

public class PageRequest {
	
	private int size;
	private int page;
	
	public PageRequest() {
	}
	public PageRequest(int size) {
		this(size,0);
	}
	public PageRequest(int size, int page) {
		setSize(size);
		setPage(page);
	}
	public int getSize() {
		return size;
	}
	public int getPage() {
		return page;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	public PageRequest next() {
		return new PageRequest(getSize(), getPage()+1);
	}
	public PageRequest previous() {
		return new PageRequest(getSize(), getPage()-1);
	}
	
}
