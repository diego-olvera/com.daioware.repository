package com.daioware.repository.util;

import java.time.LocalDateTime;

public class RepoDateUtil {

	public static void setInitialDates(LocalDateTimeItem item) {
		LocalDateTime now=LocalDateTime.now();
		item.setInsertionDate(now);
		item.setUpdateDate(now);
	}
	public static void setUpdateDate(LocalDateTimeItem item) {
		item.setUpdateDate(LocalDateTime.now());
	}

}
