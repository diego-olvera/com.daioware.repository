package com.daioware.repository.util;

public class RepoRequest {
	
	private FilterList filters;
	private SortList sorts;
	private PageRequest pageRequest;
	
	public RepoRequest() {
	}
	public RepoRequest(FilterList filters, SortList sorts, PageRequest pageRequest) {
		setFilters(filters);
		setSorts(sorts);
		setPageRequest(pageRequest);
	}
	public FilterList getFilters() {
		return filters;
	}
	public SortList getSorts() {
		return sorts;
	}
	public PageRequest getPageRequest() {
		return pageRequest;
	}
	public void setFilters(FilterList filters) {
		this.filters = filters;
	}
	public void setSorts(SortList sorts) {
		this.sorts = sorts;
	}
	public void setPageRequest(PageRequest pageRequest) {
		this.pageRequest = pageRequest;
	}
	
}
