package com.daioware.repository.util;

public abstract class SaveException extends Exception{
	private static final long serialVersionUID = 1L;

	public SaveException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SaveException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SaveException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SaveException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SaveException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	
}
