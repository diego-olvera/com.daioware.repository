package com.daioware.repository.util;

public class Sort {
	
	public static enum Direction{
		ASC,
		DESC
	}
	
	private String member;
	private Direction direction;
	
	public Sort() {
	}
	
	public Sort(String member, Direction direction) {
		this.member = member;
		this.direction = direction;
	}
	public String getMember() {
		return member;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setMember(String member) {
		this.member = member;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	
}
