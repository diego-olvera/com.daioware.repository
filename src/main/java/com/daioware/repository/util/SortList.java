package com.daioware.repository.util;

import java.util.List;

public class SortList {
	private List<Sort> sorts;

	public List<Sort> getSorts() {
		return sorts;
	}

	public void setSorts(List<Sort> sorts) {
		this.sorts = sorts;
	}
	
	
}
